<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    protected $table = 'projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'team_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Permission
     * @return Collection
     */
    public function permission()
    {
        return $this->hasOne(ProjectPermission::class);
    }

    /**
     * Permissions
     * @return Collection
     */
    public function permissions()
    {
        return $this->hasMany(ProjectPermission::class);
    }

    /**
     * Team
     * @return Collection
     */
    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * Campaigns
     * @return Collection
     */
    public function campaigns()
    {
        return $this->hasMany(Campaign::class);
    }
}
