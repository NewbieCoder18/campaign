<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignPermission extends Model
{
    protected $table = 'campaign_permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'campaign_id', 'user_id', 'role_id'
    ];

    /**
     * Campaign
     * @return Collection
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    /**
     * User
     * @return Collection
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Role
     * @return Collection
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
