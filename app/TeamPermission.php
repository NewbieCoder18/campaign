<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamPermission extends Model
{
    protected $table = 'team_permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_id', 'user_id', 'role_id'
    ];

    /**
     * Team
     * @return Collection
     */
    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * User
     * @return Collection
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Role
     * @return Collection
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
