<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Hashers\CampaignHasher;
use App\Campaign;
use App\Helpers\{TimeHelper, MemberHelper};

class CampaignTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Campaign $campaign)
    {
        return [
            'id' => CampaignHasher::encode($campaign->id),
            'name' => $campaign->name,
            'type' => $campaign->template->name,
            'status' => is_null($campaign->status) ? 'Draft' : $campaign->status,
            'created_at' => date('d M Y', strtotime($campaign->created_at)),
            'updated_at_relative' => TimeHelper::findTimeAgo(date('Y-m-d H:i:s', strtotime($campaign->updated_at))),
            'updated_at' => $campaign->updated_at,
            'members' => MemberHelper::campaign($campaign)
        ];
    }
}
