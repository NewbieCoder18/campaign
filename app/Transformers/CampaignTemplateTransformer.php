<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Hashers\CampaignTemplateHasher;
use App\CampaignTemplate;

class CampaignTemplateTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(CampaignTemplate $template)
    {
        return [
            'id' => CampaignTemplateHasher::encode($template->id),
            'name' => $template->name,
            'slug' => $template->slug
        ];
    }
}
