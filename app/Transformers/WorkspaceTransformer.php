<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Hashers\WorkspaceHasher;
use App\Workspace;

class WorkspaceTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Workspace $workspace)
    {
        return [
            'id' => WorkspaceHasher::encode($workspace->id),
            'name' => $workspace->name,
            'created_at' => $workspace->created_at,
            'updated_at' => $workspace->updated_at
        ];
    }
}
