<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Invitation;
use App\Campaign;
use App\Notifications\CampaignInvitation;

class ProcessCampaignInvitation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $invitation;
    protected $campaign;
    protected $isRegisteredUser;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Invitation $invitation, Campaign $campaign, $isRegisteredUser)
    {
        $this->invitation = $invitation;
        $this->campaign = $campaign;
        $this->isRegisteredUser = $isRegisteredUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->invitation->notify(new CampaignInvitation($this->campaign, $this->invitation, $this->isRegisteredUser));
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
