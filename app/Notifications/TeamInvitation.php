<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Hashers\TeamHasher;
use App\Hashers\InvitationHasher;

class TeamInvitation extends Notification
{
    use Queueable;

    protected $team;
    protected $invitation;
    protected $isRegisteredUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($team, $invitation, $isRegisteredUser)
    {
        $this->team = $team;
        $this->invitation = $invitation;
        $this->isRegisteredUser = $isRegisteredUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->isRegisteredUser) {
            return $this->toRegisteredUser();
        } else {
            return $this->toUnregisteredUser();
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Send invitation email to registered user
     */
    private function toRegisteredUser()
    {
        $url = url(sprintf('/team/%s', TeamHasher::encode($this->team->id)));

        return (new MailMessage)
                    ->subject(sprintf(
                        '%s added you to "%s"',
                        $this->team->permission->user->name,
                        $this->team->name
                    ))
                    ->line(sprintf(
                        '%s added you to %s team',
                        $this->team->permission->user->name,
                        $this->team->name
                    ))
                    ->line('Welcome to the team')
                    ->action('View Team', $url);
    }

    /**
     * Send invitation email to unregistered user
     */
    private function toUnregisteredUser()
    {
        $url = url(sprintf('/invitation/%s', InvitationHasher::encode($this->invitation->id)));

        return (new MailMessage)
                    ->subject(sprintf(
                        '%s invited you to join "%s"',
                        $this->team->permission->user->name,
                        $this->team->name
                    ))
                    ->line(sprintf(
                        '%s invited you to join %s',
                        $this->team->permission->user->name,
                        $this->team->name
                    ))
                    ->action('Accept Invite', $url);
    }
}
