<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Hashers\CampaignHasher;
use App\Hashers\InvitationHasher;

class CampaignInvitation extends Notification
{
    use Queueable;

    protected $campaign;
    protected $invitation;
    protected $isRegisteredUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($campaign, $invitation, $isRegisteredUser)
    {
        $this->campaign = $campaign;
        $this->invitation = $invitation;
        $this->isRegisteredUser = $isRegisteredUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->isRegisteredUser) {
            return $this->toRegisteredUser();
        } else {
            return $this->toUnregisteredUser();
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Send invitation email to registered user
     */
    private function toRegisteredUser()
    {
        $url = url(sprintf('/campaign/%s', CampaignHasher::encode($this->campaign->id)));

        return (new MailMessage)
                    ->subject(sprintf(
                        '%s added you to "%s"',
                        $this->campaign->permission->user->name,
                        $this->campaign->name
                    ))
                    ->line(sprintf(
                        '%s added you to %s campaign',
                        $this->campaign->permission->user->name,
                        $this->campaign->name
                    ))
                    ->line('Welcome to the campaign')
                    ->action('View Campaign', $url);
    }

    /**
     * Send invitation email to unregistered user
     */
    private function toUnregisteredUser()
    {
        $url = url(sprintf('/invitation/%s', InvitationHasher::encode($this->invitation->id)));
        
        return (new MailMessage)
                    ->subject(sprintf(
                        '%s invited you to join "%s"',
                        $this->campaign->permission->user->name,
                        $this->campaign->name
                    ))
                    ->line(sprintf(
                        '%s invited you to join %s',
                        $this->campaign->permission->user->name,
                        $this->campaign->name
                    ))
                    ->action('Accept Invite', $url);
    }
}
