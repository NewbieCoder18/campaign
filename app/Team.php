<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use SoftDeletes;

    protected $table = 'teams';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'workspace_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Permission
     * @return Collection
     */
    public function permission()
    {
        return $this->hasOne(TeamPermission::class);
    }

    /**
     * Permissions
     * @return Collection
     */
    public function permissions()
    {
        return $this->hasMany(TeamPermission::class);
    }

    /**
     * Workspace
     * @return Collection
     */
    public function workspace()
    {
        return $this->belongsTo(Workspace::class);
    }

    /**
     * Projects
     * @return Collection
     */
    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
