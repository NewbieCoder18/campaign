<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\Utility\{IsFirstTimeLoggedIn, GetFirstWorkspace};
use App\Http\Requests\API\Utility\{GetFirstTeam, GetFirstProject};
use App\{Workspace, Team};

class UtilityController extends Controller
{
    /**
     * Check if the user is logged in for the first time
     * @param null
     * @return bool
     */
    public function isFirstTimeLoggedIn(IsFirstTimeLoggedIn $request) : bool
    {
        return $request->commit();
    }

    /**
     * Get first workspace unique id for logged in user
     * @param null
     * @return string
     */
    public function getFirstWorkspace(GetFirstWorkspace $request) : string
    {
        return $request->commit();
    }

    /**
     * Get first team unique id for logged in user
     * @param null
     * @return string
     */
    public function getFirstTeam(Workspace $workspace, GetFirstTeam $request) : string
    {
        return $request->commit($workspace);
    }

    /**
     * Get first project unique id for logged in user
     * @param null
     * @return string
     */
    public function getFirstProject(Team $team, GetFirstProject $request) : string
    {
        return $request->commit($team);
    }
}
