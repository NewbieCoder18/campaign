<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\Team;
use App\User;
use App\Http\Requests\API\Project\{StoreRequest, UpdateRequest};
use App\Http\Requests\API\Project\{ShowRequest, DeleteRequest};
use App\Http\Requests\API\Project\{MemberRequest, CampaignRequest};
use App\Http\Requests\API\Project\RevokeRequest;

class ProjectController extends Controller
{
    /**
     * Store Project.
     * @param Request
     * @return Project
     */
    public function store(Team $team, StoreRequest $request)
    {
        return $request->commit($team);
    }

    /**
     * Update Project.
     * @param Request
     * @return Project
     */
    public function update(Project $project, UpdateRequest $request)
    {
        return $request->commit($project);
    }

    /**
     * Show Project.
     * @param Project
     * @return Project
     */
    public function show(Project $project, ShowRequest $request)
    {
        return $request->commit($project);
    }

    /**
     * Delete Project.
     * @param Project
     * @return null
     */
    public function delete(Project $project, DeleteRequest $request)
    {
        return $request->commit($project);
    }

    /**
     * Members of Projects.
     * @param Project
     * @return Collection
     */
    public function members(Project $project, MemberRequest $request)
    {
        return $request->commit($project);
    }

    /**
     * Campaigns of Project.
     * @param Project
     * @return array
     */
    public function campaigns(Project $project, CampaignRequest $request)
    {
        return $request->commit($project);
    }

    /**
     * Revoke user from project.
     * @param Project
     * @param User
     * @return null
     */
    public function revoke(Project $project, User $user, RevokeRequest $request)
    {
        return $request->commit($project, $user);
    }
}
