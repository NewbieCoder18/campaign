<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Workspace;
use App\Http\Requests\API\Link\LinkRequest;

class LinkController extends Controller
{
    /**
     * Generate links.
     * Return list of teams with its projects.
     * @param Workspace
     * @return Collection
     */
    public function links(Workspace $workspace, LinkRequest $request)
    {
        return $request->commit($workspace);
    }
}
