<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Workspace;
use App\Team;
use App\Project;
use App\Campaign;
use App\Http\Requests\API\Invitation\{WorkspaceRequest, TeamRequest};
use App\Http\Requests\API\Invitation\{ProjectRequest, CampaignRequest};

class InvitationController extends Controller
{
    /**
     * @ Send Invitation.
     * @ Workspace
     */
    public function sendInvitationToWorkspace(Workspace $workspace, WorkspaceRequest $request)
    {
        return $request->commit($workspace);
    }

    /**
     * @ Send Invitation.
     * @ Team
     */
    public function sendInvitationToTeam(Team $team, TeamRequest $request)
    {
        return $request->commit($team);
    }

    /**
     * @ Send Invitation.
     * @ Project
     */
    public function sendInvitationToProject(Project $project, ProjectRequest $request)
    {
        return $request->commit($project);
    }

    /**
     * @ Send Invitation.
     * @ Campaign
     */
    public function sendInvitationToCampaign(Campaign $campaign, CampaignRequest $request)
    {
        return $request->commit($campaign);
    }
}
