<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use App\Http\Requests\API\AccessControl\Role\{ListRequest, StoreRequest};
use App\Http\Requests\API\AccessControl\Role\{ShowRequest, UpdateRequest};
use App\Http\Requests\API\AccessControl\Role\DeleteRequest;

class RoleController extends Controller
{
    /**
     * Protect controller with middleware.
     */
    public function __construct()
    {
        $this->middleware(['role:admin']);
    }

    /**
     * List of Roles.
     * @param null
     * @return Roles
     */
    public function list(ListRequest $request)
    {
        return $request->commit();
    }

    /**
     * Store Role.
     * @param Request
     * @return Role
     */
    public function store(StoreRequest $request)
    {
        return $request->commit();
    }

    /**
     * Show Role.
     * @param Role
     * @return Role
     */
    public function show(Role $role, ShowRequest $request)
    {
        return $request->commit($role);
    }

    /**
     * Update Role.
     * @param Request
     * @return Role
     */
    public function update(Role $role, UpdateRequest $request)
    {
        return $request->commit($role);
    }

    /**
     * Delete Role.
     * @param Role
     * @return null
     */
    public function delete(Role $role, DeleteRequest $request)
    {
        return $request->commit($role);
    }
}
