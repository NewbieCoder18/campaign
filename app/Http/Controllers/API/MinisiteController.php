<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Campaign;
use App\Http\Requests\API\Minisite\{ListRequest, UpdateMinisiteTemplateRequest};
use App\Http\Requests\API\Minisite\{StoreRequest, GetFirstMinisiteRequest};

class MinisiteController extends Controller
{
    /**
     * Get campaign minisites.
     * @param Campaign
     * @return array
     */
    public function list(Campaign $campaign, ListRequest $request)
    {
        return $request->commit($campaign);
    }

    /**
     * Update minisite template.
     * @return array
     */
    public function updateMinisiteTemplate(UpdateMinisiteTemplateRequest $request)
    {
        return $request->commit();
    }

    /**
     * Store minisite.
     * @param Campaign
     * @return array
     */
    public function store(Campaign $campaign, StoreRequest $request)
    {
        return $request->commit($campaign);
    }

    /**
     * Get first minisite.
     * @param Campaign
     * @return array
     */
    public function getFirstMinisite(Campaign $campaign, GetFirstMinisiteRequest $request)
    {
        return $request->commit($campaign);
    }
}
