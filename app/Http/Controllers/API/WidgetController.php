<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\Widget\{ListRequest, StoreRequest};
use App\Http\Requests\API\Widget\{UpdateRequest, ShowRequest};
use App\{Minisite, Widget};

class WidgetController extends Controller
{
    /**
     * List widgets in minisite.
     * @param Minisite
     * @return array
     */
    public function list(Minisite $minisite, ListRequest $request)
    {
        return $request->commit($minisite);
    }

    /**
     * Store widget.
     * @param Minisite
     * @return array
     */
    public function store(Minisite $minisite, StoreRequest $request)
    {
        return $request->commit($minisite);
    }

    /**
     * Update widget.
     * @param Widget
     * @return array
     */
    public function update(Widget $widget, UpdateRequest $request)
    {
        return $request->commit($widget);
    }

    /**
     * Show widget.
     * @param Widget
     * @return array
     */
    public function show(Widget $widget, ShowRequest $request)
    {
        return $request->commit($widget);
    }
}
