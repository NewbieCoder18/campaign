<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use App\User;
use DB;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required'
        ];

        if ($this->email !== $this->user->email) {
            $rules['email'] = 'required|email|unique:users,email';
        }

        return $rules;
    }

    /**
     * Update User.
     * @return User
     */
    public function commit(User $user) : User
    {
        $user->update(['name' => $this->name, 'email' => $this->email]);

        DB::table('model_has_roles')->whereModelId($user->id)->delete();

        $user->assignRole($this->roles);

        return $user;
    }
}
