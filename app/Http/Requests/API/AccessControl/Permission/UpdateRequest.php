<?php

namespace App\Http\Requests\API\AccessControl\Permission;

use Illuminate\Foundation\Http\FormRequest;
use Spatie\Permission\Models\Permission;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'guard_name' => 'required'
        ];
    }

    /**
     * Update Permission.
     * @return Permission
     */
    public function commit(Permission $permission) : Permission
    {
        $permission->update([
            'name' => $this->name,
            'guard_name' => $this->guard_name
        ]);

        return $permission;
    }
}
