<?php

namespace App\Http\Requests\API\Profile;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required'
        ];

        if ($this->email !== Auth::user()->email) {
            $rules['email'] = 'email|required|unique:users';
        }

        return $rules;
    }

    /**
     * Update profile.
     * @return null
     */
    public function commit()
    {
        Auth::user()->update(['name' => $this->name, 'email' => $this->email]);
    }
}
