<?php

namespace App\Http\Requests\API\Campaign;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Database\Eloquent\Collection;
use App\CampaignTemplate;
use App\Transformers\CampaignTemplateTransformer;

class TemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Campaign Templates.
     * @return array
     */
    public function commit() : array
    {
        return fractal(CampaignTemplate::all(), CampaignTemplateTransformer::class)->toArray()['data'];
    }
}
