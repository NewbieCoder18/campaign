<?php

namespace App\Http\Requests\API\Minisite;

use Illuminate\Foundation\Http\FormRequest;
use App\Transformers\MinisiteTransformer;
use App\Campaign;

class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Get Campaign minisites.
     * @param Campaign
     * @return array
     */
    public function commit(Campaign $campaign) : array
    {
        return fractal($campaign->minisites, MinisiteTransformer::class)->toArray()['data'];
    }
}
