<?php

namespace App\Http\Requests\API\Minisite;

use Illuminate\Foundation\Http\FormRequest;
use App\Transformers\MinisiteTransformer;
use App\{Campaign, Minisite};

class GetFirstMinisiteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Get first minisite.
     * @param Campaign
     * @return array
     */
    public function commit(Campaign $campaign) : array
    {
        $minisite = Minisite::whereCampaignId($campaign->id)->whereFirst(1)->first();

        return fractal($minisite, MinisiteTransformer::class)->toArray()['data'];
    }
}
