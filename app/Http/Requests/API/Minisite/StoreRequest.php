<?php

namespace App\Http\Requests\API\Minisite;

use Illuminate\Foundation\Http\FormRequest;
use App\{Campaign, Minisite};
use App\Transformers\MinisiteTransformer;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Store minisite.
     * @return array
     */
    public function commit(Campaign $campaign) : array
    {
        $minisite = new Minisite();
        $minisite->campaign_id = $campaign->id;
        $minisite->name = $this->name;
        $minisite->sort = $this->sort;
        $minisite->type = $this->type;
        $minisite->first = $this->first;
        $minisite->force_redirect = $this->force_redirect;
        $minisite->url = $this->url;
        $minisite->is_protected_by_optin = $this->is_protected_by_optin;
        $minisite->optin = $this->optin;
        $minisite->next = $this->next;
        $minisite->campaign_target_id = $this->campaign_target_id;
        $minisite->styles = $this->styles;
        $minisite->header = $this->header;
        $minisite->content = $this->content;
        $minisite->footer = $this->footer;
        $minisite->save();

        return fractal($minisite, MinisiteTransformer::class)->toArray()['data'];
    }
}
