<?php

namespace App\Http\Requests\API\Widget;

use Illuminate\Foundation\Http\FormRequest;
use App\Transformers\WidgetTransformer;
use App\Widget;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Update widget.
     * @param Widget
     * @return array
     */
    public function commit(Widget $widget) : array
    {
        $widget->target = !is_null($this->target) ? $this->target : null;
        $widget->content = $this->json_content;
        $widget->update();

        return fractal($widget, WidgetTransformer::class)->toArray()['data'];
    }
}
