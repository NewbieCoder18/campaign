<?php

namespace App\Http\Requests\API\Workspace;

use Illuminate\Foundation\Http\FormRequest;
use App\Workspace;
use App\Transformers\TeamTransformer;

class TeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Teams of Workspace.
     * @return array
     */
    public function commit(Workspace $workspace) : array
    {
        return fractal($workspace->teams, TeamTransformer::class)->toArray()['data'];
    }
}
