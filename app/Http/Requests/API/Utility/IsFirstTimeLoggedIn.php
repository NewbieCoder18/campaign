<?php

namespace App\Http\Requests\API\Utility;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class IsFirstTimeLoggedIn extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Check if the user is logged in for the first time
     * @param null
     * @return bool
     */
    public function commit() : bool
    {
        $isFirstTimeLoggedIn = true;

        if (!is_null(Auth::user()->last_logged_in)) {
            $isFirstTimeLoggedIn = false;
        }

        return $isFirstTimeLoggedIn;
    }
}
