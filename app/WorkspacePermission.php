<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkspacePermission extends Model
{
    protected $table = 'workspace_permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'workspace_id', 'user_id', 'role_id'
    ];

    /**
     * Workspace
     * @return Collection
     */
    public function workspace()
    {
        return $this->belongsTo(Workspace::class);
    }

    /**
     * User
     * @return Collection
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Role
     * @return Collection
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
