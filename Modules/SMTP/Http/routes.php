<?php

/**
 * Web Routes
 */
Route::group([
    'middleware' => 'web',
    'namespace' => 'Modules\SMTP\Http\Controllers'
],
function () {
    Route::get('smtp', 'SMTPController@index');
});


/**
 * API Routes
 */
Route::group([
    'middleware' => 'auth:api',
    'namespace' => 'Modules\SMTP\Http\Controllers',
    'prefix' => 'api'
],
function () {
    Route::get('smtps', 'SMTPController@smtps');
    Route::get('smtp/{smtpid}', 'SMTPController@show');
    Route::post('smtp', 'SMTPController@store');
    Route::put('smtp/{smtpid}', 'SMTPController@update');
    Route::delete('smtp/{smtpid}', 'SMTPController@delete');
});


