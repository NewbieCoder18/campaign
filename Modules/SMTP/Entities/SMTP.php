<?php

namespace Modules\SMTP\Entities;

use Illuminate\Database\Eloquent\Model;

class SMTP extends Model
{
    protected $table = 'smtps';
    protected $fillable = [
        'name',
        'host',
        'port',
        'username',
        'password',
        'encryption',
        'from_email',
        'from_name'
    ];
}
