<?php

namespace Modules\Integration\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use DB;
use Modules\Integration\Entities\Contact;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Helpers\MailChimp;

class ProcessImportContactMailChimp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $list;
    protected $offset;
    protected $count;
    protected $apiKey;
    protected $importJobID;

    public $tries = 2;
    public $timeout = 600;

    public function __construct($list, $offset, $count, $apiKey, $importJobID)
    {
        $this->list = $list;
        $this->offset = $offset;
        $this->count = $count;
        $this->apiKey = $apiKey;
        $this->importJobID = $importJobID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo "START\n";

        $service = new MailChimp($this->apiKey);
        $importJob = ImportJob::find($this->importJobID);

        try {
            $listMembers = $service->contacts($this->list, $this->offset, $this->count);
            $contacts = $listMembers['members'];
            $total = $listMembers['total_items'];

            echo "TOTAL: $total\n";

            if ($total > 0) {
                DB::beginTransaction();

                foreach ($contacts as $contact) {
                    echo sprintf("ADD %s\n", $contact['email_address']);

                    Contact::insert([
                        'import_job_id' => $this->importJobID,
                        'full_name' => sprintf('%s %s', $contact['merge_fields']['FNAME'], $contact['merge_fields']['LNAME']),
                        'first_name' => $contact['merge_fields']['FNAME'],
                        'last_name' => $contact['merge_fields']['LNAME'],
                        'email' => $contact['email_address'],
                        'phone' => isset($contact['merge_fields']['PHONE']) ? $contact['merge_fields']['PHONE'] : '',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                    DB::commit();
                }
            }

            if (($this->offset + $this->count) < $total) {
                echo "RE-DISPATCH\n";

                $this->dispatch(
                    $this->list,
                    $this->offset + $this->count,
                    $this->count,
                    $this->apiKey,
                    $this->importJobID
                )->delay(now()->addSeconds(10));
            }

            $importJob->total = $importJob->total + count($contacts);
            $importJob->update();

            if ($importJob->total == $total) {
                $importJob->status = 'COMPLETED';
                $importJob->update();
            }

            echo "FINISH\n";
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
