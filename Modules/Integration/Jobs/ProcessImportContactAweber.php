<?php

namespace Modules\Integration\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use DB;
use Modules\Integration\Entities\Contact;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Helpers\Aweber;

class ProcessImportContactAweber implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $list;
    protected $start;
    protected $size;
    protected $token;
    protected $importJobID;

    public $tries = 2;
    public $timeout = 600;

    public function __construct($list, $start, $size, $token, $importJobID)
    {
        $this->list = $list;
        $this->start = $start;
        $this->size = $size;
        $this->token = $token;
        $this->importJobID = $importJobID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo "START\n";

        $service = new Aweber($this->token);
        $importJob = ImportJob::find($this->importJobID);

        try {
            $listMembers = $service->contacts($this->list, $this->start, $this->size);
            $contacts = $listMembers->data['entries'];
            $total = $listMembers->data['total_size'];

            echo "TOTAL: $total\n";

            if (count($contacts) > 0) {
                DB::beginTransaction();

                foreach ($contacts as $contact) {
                    echo sprintf("ADD %s\n", $contact['email']);

                    Contact::insert([
                        'import_job_id' => $this->importJobID,
                        'full_name' => $contact['name'],
                        'first_name' => $contact['name'],
                        'last_name' => '',
                        'email' => $contact['email'],
                        'phone' => isset($contact['custom_fields']['phone']) ? $contact['custom_fields']['phone'] : '',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                    DB::commit();
                }
            }

            if (($this->start + $this->size) < $total) {
                echo "RE-DISPATCH\n";

                $this->dispatch(
                    $this->list,
                    $this->start + $this->size,
                    $this->size,
                    $this->token,
                    $this->importJobID
                )->delay(now()->addSeconds(10));
            }

            $importJob->total = $importJob->total + count($contacts);
            $importJob->update();

            if ($importJob->total == $total) {
                $importJob->status = 'COMPLETED';
                $importJob->update();
            }

            echo "FINISH\n";
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
