<?php

namespace Modules\Integration\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use DB;
use Modules\Integration\Entities\Contact;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Helpers\MadMimi;

class ProcessImportContactMadMimi implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $list;
    protected $perPage;
    protected $page;
    protected $email;
    protected $apiKey;
    protected $importJobID;

    public $tries = 2;
    public $timeout = 600;

    public function __construct($list, $perPage, $page, $email, $apiKey, $importJobID)
    {
        $this->list = $list;
        $this->perPage = $perPage;
        $this->page = $page;
        $this->email = $email;
        $this->apiKey = $apiKey;
        $this->importJobID = $importJobID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo "START\n";

        $service = new MadMimi($this->email, $this->apiKey);
        $importJob = ImportJob::find($this->importJobID);

        try {
            $lists = $service->contacts($this->list, $this->perPage, $this->page);
            $contacts = $lists->audience;
            $total = $lists->count;

            echo "TOTAL: $total\n";

            if ($total > 0) {
                DB::beginTransaction();

                foreach ($contacts as $contact) {
                    echo sprintf("ADD %s\n", $contact->columns->email);

                    Contact::insert([
                        'import_job_id' => $this->importJobID,
                        'full_name' => sprintf(
                            '%s %s',
                            $contact->columns->first_name,
                            $contact->columns->last_name
                        ),
                        'first_name' => $contact->columns->first_name,
                        'last_name' => $contact->columns->last_name,
                        'email' => $contact->columns->email,
                        'phone' => $contact->columns->phone,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                    DB::commit();
                }
            }

            if ($lists->total_pages > $this->page) {
                echo "RE-DISPATCH\n";

                $this->dispatch(
                    $this->list,
                    $this->perPage,
                    $this->page + 1,
                    $this->email,
                    $this->apiKey,
                    $this->importJobID
                )->delay(now()->addSeconds(10));
            }

            $importJob->total = $importJob->total + count($contacts);
            $importJob->update();

            if ($importJob->total == $total) {
                $importJob->status = 'COMPLETED';
                $importJob->update();
            }

            echo "FINISH\n";
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
