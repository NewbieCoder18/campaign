<?php

namespace Modules\Integration\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use DB;
use Modules\Integration\Entities\Contact;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Helpers\Infusionsoft;
use Modules\Infusionsoft\MailProviderInfusionsoft;

class ProcessImportContactInfusionsoft implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $list;
    protected $offset;
    protected $limit;
    protected $userIntegration;
    protected $importJobID;

    public $tries = 2;
    public $timeout = 600;

    public function __construct($list, $offset, $limit, $userIntegration, $importJobID)
    {
        $this->list = $list;
        $this->offset = $offset;
        $this->limit = $limit;
        $this->userIntegration = $userIntegration;
        $this->importJobID = $importJobID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo "START\n";

        $credential = json_decode($this->userIntegration->infusionsoft);
        $protocol = new MailProviderInfusionsoft(
            env('INFUSIONSOFT_CLIENT_ID'),
            env('INFUSIONSOFT_SECRET'),
            env('INFUSIONSOFT_REDIRECT_URL')
        );
        $protocol->setToken($credential->token);

        if ($protocol->isTokenExpired()) {
            echo "TOKEN EXPIRED, REQUEST NEW TOKEN...\n";

            $newToken = $protocol->setAndRefreshToken($credential->token);

            $this->userIntegration->infusionsoft = json_encode(['token' => $newToken]);
            $this->userIntegration->update();

            $protocol->setToken($newToken);

            echo "TOKEN UPDATED\n";
        }

        $service = new Infusionsoft($protocol);
        $importJob = ImportJob::find($this->importJobID);

        try {
            $listMembers = $service->contacts($this->list, $this->offset, $this->limit);
            $contacts = $listMembers->contacts;
            $total = $listMembers->count;

            echo "TOTAL: $total\n";

            if ($total > 0) {
                DB::beginTransaction();

                foreach ($contacts as $contact) {
                    echo sprintf("ADD %s\n", $contact['contact']['email']);

                    Contact::insert([
                        'import_job_id' => $this->importJobID,
                        'full_name' => sprintf('%s %s', $contact['contact']['first_name'], $contact['contact']['last_name']),
                        'first_name' => $contact['contact']['first_name'],
                        'last_name' => $contact['contact']['last_name'],
                        'email' => $contact['contact']['email'],
                        'phone' => '',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                    DB::commit();
                }
            }

            if (($this->offset + $this->limit) < $total) {
                echo "RE-DISPATCH\n";

                $this->dispatch(
                    $this->list,
                    $this->offset + $this->limit,
                    $this->limit,
                    $this->userIntegration,
                    $this->importJobID
                )->delay(now()->addSeconds(10));
            }

            $importJob->total = $importJob->total + count($contacts);
            $importJob->update();

            if ($importJob->total == $total) {
                $importJob->status = 'COMPLETED';
                $importJob->update();
            }

            echo "FINISH\n";
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
