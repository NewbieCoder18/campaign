<?php

namespace Modules\Integration\Helpers\Queues;

use Auth;
use Modules\Integration\Helpers\Queues\Queue;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Jobs\ProcessImportContactInfusionsoft;

class Infusionsoft extends Queue
{
    public function import($list, $userIntegration)
    {
        $importJob = new ImportJob();
        $importJob->user_id = Auth::id();
        $importJob->list_id = $list;
        $importJob->provider = 'infusionsoft';
        $importJob->status = 'ONPROGRESS';
        $importJob->save();

        ProcessImportContactInfusionsoft::dispatch(
            $list,
            0,
            100,
            $userIntegration,
            $importJob->id
        );

        return $importJob;
    }
}
