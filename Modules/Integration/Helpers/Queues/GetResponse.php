<?php

namespace Modules\Integration\Helpers\Queues;

use Auth;
use Modules\Integration\Helpers\Queues\Queue;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Jobs\ProcessImportContactGetResponse;

class GetResponse extends Queue
{
    public function import($list, $apiKey)
    {
        $importJob = new ImportJob();
        $importJob->user_id = Auth::id();
        $importJob->list_id = $list;
        $importJob->provider = 'getresponse';
        $importJob->status = 'ONPROGRESS';
        $importJob->save();

        ProcessImportContactGetResponse::dispatch(
            $list,
            100,
            1,
            $apiKey,
            $importJob->id
        );

        return $importJob;
    }
}
