<?php

namespace Modules\Integration\Helpers\Queues;

use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Entities\Contact;

class Queue
{
    public function status($importJobID)
    {
        try {
            $importJob = ImportJob::findOrFail($importJobID);
        } catch (\Exception $e) {
            return $e;
        }

        return $importJob;
    }

    public function contacts($importJobID)
    {
        try {
            $contacts = ImportJob::findOrFail($importJobID)->contacts;
        } catch (\Exception $e) {
            return $e;
        }

        return $contacts;
    }

    public function delete($importJobID)
    {
        try {
            Contact::whereImportJobId($importJobID)->delete();
        } catch (\Exception $e) {
            return $e;
        }

        return 'OK';
    }
}
