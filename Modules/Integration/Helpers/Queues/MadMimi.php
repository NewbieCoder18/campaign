<?php

namespace Modules\Integration\Helpers\Queues;

use Auth;
use Modules\Integration\Helpers\Queues\Queue;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Jobs\ProcessImportContactMadMimi;

class MadMimi extends Queue
{
    public function import($list, $email, $apiKey)
    {
        $importJob = new ImportJob();
        $importJob->user_id = Auth::id();
        $importJob->list_id = $list;
        $importJob->provider = 'madmimi';
        $importJob->status = 'ONPROGRESS';
        $importJob->save();

        ProcessImportContactMadMimi::dispatch(
            $list,
            100,
            1,
            $email,
            $apiKey,
            $importJob->id
        );

        return $importJob;
    }
}
