<?php

namespace Modules\Integration\Helpers;

class VideoMailbot
{
    protected $apiKey;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function lists()
    {
        $videoMailbotLists = [];
        try {
            $client = new \GuzzleHttp\Client(['base_uri' => 'http://videomailbot.com']);
            $response = $client->request('GET', '/v1/api/lists?apiKey=' . $this->apiKey);
            $_ = json_decode($response->getBody()->getContents());
            if (isset($_->lists)) {
                if (count($_->lists > 0)) {
                    foreach ($_->lists as $list) {
                        $videoMailbotLists[$list->id] = $list->name;
                    }
                }
            }
        } catch (\Exception $e) {
            return $e;
        }

        return $videoMailbotLists;
    }

    public function subscribe($list, $request)
    {
        try {
            $p = [
                'apiKey' => $this->apiKey,
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'email' => $request->email,
                'tags' => [$list]
            ];
            $client = new \GuzzleHttp\Client(['base_uri' => 'http://videomailbot.com']);
            $subscribe = $client->request('POST', '/v1/api/subscribe', ['json' => $p]);
        } catch (\Exception $e) {
            return $e;
        }

        return $subscribe;
    }
}
