<?php

namespace Modules\Integration\Helpers;

class Sendlane
{
    protected $apiKey;
    protected $hashKey;
    protected $domain;

    public function __construct($apiKey, $hashKey, $domain)
    {
        $this->apiKey = $apiKey;
        $this->hashKey = $hashKey;
        $this->domain = $domain;
    }

    public function lists()
    {
        $sendlaneLists = [];
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, sprintf('https://%s/api/v1/lists', $this->domain));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, sprintf('api=%s&hash=%s', $this->apiKey, $this->hashKey));
            $result = curl_exec($ch);
            $lists = json_decode($result);
            if (!is_null($lists) & count($lists) > 0) {
                foreach ($lists as $list) {
                    $sendlaneLists[$list->list_id] = $list->list_name;
                }
            }
        } catch (\Exception $e) {
            return $e;
        }
        
        return $sendlaneLists;
    }

    public function subscribe($list, $request)
    {
        try {
            $subscriber = $request->firstName . ' ' . $request->lastName . '<' . $request->email . '>';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, sprintf('https://%s/api/v1/list-subscribers-add', $this->domain));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, sprintf('api=%s&hash=%s&email=%s&list_id=%s', $this->apiKey, $this->hashKey, $subscriber, $list));
            $subscribe = curl_exec($ch);
        } catch (\Exception $e) {
            return $e;
        }

        return $subscribe;
    }
}
