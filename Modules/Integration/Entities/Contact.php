<?php

namespace Modules\Integration\Entities;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'import_job_id',
        'full_name',
        'first_name',
        'last_name',
        'email',
        'phone',
        'misc'
    ];

    protected $table = 'cwa_imported_contacts';
}
