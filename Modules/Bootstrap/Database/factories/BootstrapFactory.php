<?php

use Faker\Generator as Faker;

$factory->define(Modules\Bootstrap\Entities\Bootstrap::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'text' => $faker->text,
    ];
});
