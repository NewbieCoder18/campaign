export default [
  {
    name: 'Dashboard',
    icon: 'ti-panel',
    path: '/bootstrap/overview'
  },
  {
    name: 'Dummy Data',
    icon: 'ti-widgetized',
    path: '/bootstrap/dummy'
  }
]
