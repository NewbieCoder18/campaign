<nav class="navbar navbar-absolute">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand">
        <a href="/locale/en">
          <span class="flag-icon flag-icon-gb"></span>
        </a>&nbsp;
        <a href="/locale/id">
          <span class="flag-icon flag-icon-id"></span>
        </a>
      </div>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li>
          @if (Request::is('login'))
            <a href="{{ route('register') }}">
              {{ __('auth.login.register') }}
            </a>
          @else
            <a href="{{ route('login') }}">
              {{ __('auth.login.title') }}
            </a>
          @endif
        </li>
      </ul>
    </div>
  </div>
</nav>
