<script src="/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/bootstrap-switch-tags.js"></script>
<script src="/js/paper-dashboard.js" type="text/javascript"></script>

<!--  Forms Validations Plugin -->
<script src="/js/jquery.validate.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/localization/messages_id.js"></script> -->

<script type="text/javascript">
  $().ready(function () {
    $('#loginForm, #registerForm, #emailForm, #resetForm').validate();
  });
</script>
