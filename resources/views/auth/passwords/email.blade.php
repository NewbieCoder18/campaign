@extends('layouts.app')

@section('title')
{{ __('auth.forgot_password.title') }}
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
      @if (session('status'))
        <div class="alert alert-success" role="alert">
          {{ session('status') }}
        </div>
      @endif

      <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('auth.forgot_password.label') }}"
        id="emailForm">
        <div class="card" data-background="color" data-color="blue">
          <div class="card-header">
            <h3 class="card-title">{{ __('auth.forgot_password.title') }}</h3>
            <a href="/" class="logo"><img src="/img/{{ env('IMAGE_ASSET_DIRECTORY') }}/white.png"/></a>
          </div>
          <div class="card-content">
            @csrf
            <div class="form-group" style="margin-bottom:5px;">
              <label>{{ __('auth.forgot_password.email_label') }}</label>
              <input id="email" type="email" name="email" value="{{ old('email') }}"
                class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} input-no-border" required autofocus>
            </div>
            @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
            @endif
          </div>
          <div class="card-footer text-center">
            <button type="submit" class="btn btn-fill btn-wd">{{ __('auth.forgot_password.button') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
