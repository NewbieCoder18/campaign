@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-6 col-md-offset-3" style="padding-left:0px;padding-right:0px;">
      <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('auth.reset_password.title') }}"
        id="resetForm">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">{{ __('auth.reset_password.title') }}</h3>
            <a href="/" class="logo"><img src="/img/{{ env('IMAGE_ASSET_DIRECTORY') }}/white.png"/></a>
          </div>
          <div class="card-content">
            <div class="form-group row">
              <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('auth.reset_password.email_label') }}</label>

              <div class="col-md-6">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>
                @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group row">
              <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('auth.reset_password.password_label') }}</label>

              <div class="col-md-6">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                @if ($errors->has('password'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group row">
              <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('auth.reset_password.confirm_password_label') }}</label>
              <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
              </div>
            </div>
          </div>
          <div class="card-footer text-right">
            <button type="submit" class="btn btn-primary">
              {{ __('auth.reset_password.button') }}
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
