import Vue from 'vue'
import './pollyfills'
import './bootstrap'
import VueRouter from 'vue-router'
import VueNotify from 'vue-notifyjs'
import VeeValidate from 'vee-validate'
import VueTour from 'vue-tour';
import vClickOutside from 'v-click-outside'
import VueCookie from 'vue-cookie'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'
import VueInternationalization from 'vue-i18n'
import Locale from './vue-i18n-locales.generated'
import App from './App.vue'

// Plugins
import GlobalComponents from './globalComponents'
import GlobalDirectives from './globalDirectives'
import SideBar from './components/UIComponents/SidebarPlugin'
import VModal from 'vue-js-modal'

// Router
import routes from './routes/routes'

// Library Imports
import './assets/sass/paper-dashboard.scss'
import './assets/sass/element_variables.scss'
import './assets/sass/demo.scss'
import 'flag-icon-css/css/flag-icon.css'

import sidebarLinks from './sidebarLinks'

require('froala-editor/js/froala_editor.pkgd.min')
require('froala-editor/css/froala_editor.pkgd.min.css')
require('font-awesome/css/font-awesome.css')
require('froala-editor/css/froala_style.min.css')

import VueFroala from 'vue-froala-wysiwyg'
import Swatches from 'vue-swatches'
import "vue-swatches/dist/vue-swatches.min.css"

// Plugin Setup
Vue.use(VueFroala)
require('vue-tour/dist/vue-tour.css')

// Fix conflict
const config = {
  fieldsBagName: 'veeFields'
}

// Plugin Setup
Vue.use(VueTour)
Vue.use(VueRouter)
Vue.use(GlobalDirectives)
Vue.use(GlobalComponents)
Vue.use(VueNotify)
Vue.use(SideBar, {sidebarLinks: sidebarLinks})
Vue.use(VeeValidate, config)
Vue.use(VModal, { dynamic: true })
locale.use(lang)
Vue.use(VueInternationalization)
Vue.use(VueCookie)
Vue.use(vClickOutside)

Vue.component(
  'swatches',
  Swatches
)

Vue.component(
  'passport-clients',
  require('./components/passport/Clients.vue')
)

Vue.component(
  'passport-authorized-clients',
  require('./components/passport/AuthorizedClients.vue')
)

Vue.component(
  'passport-personal-access-tokens',
  require('./components/passport/PersonalAccessTokens.vue')
)

const i18n = new VueInternationalization({
  locale: Vue.cookie.get('locale') ? Vue.cookie.get('locale') : 'en',
  silentTranslationWarn: true,
  messages: Locale
})

// Configure Router
const router = new VueRouter({
  mode: 'history',
  routes,
  linkActiveClass: 'active'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  router,
  i18n
}).$mount('#app')
