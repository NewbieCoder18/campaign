import { i18n } from './lang'

let navigations = [
  {
    name: i18n.t('menu.dashboard'),
    icon: 'ti-panel',
    path: '/dashboard'
  },
  {
    name: i18n.t('menu.users'),
    icon: 'ti-user',
    path: '/admin/user'
  },
  {
    name: 'Access Control',
    icon: 'ti-key',
    collapsed: true,
    children: [
      {
        name: 'Overview',
        path: '/admin/access-control/overview'
      },
      {
        name: 'Roles',
        path: '/admin/access-control/role'
      },
      {
        name: 'Permission',
        path: '/admin/access-control/permission'
      }
    ]
  },
  {
    name: 'Media Library',
    icon: 'ti-image',
    path: '/media-library'
  },
  {
    name: 'Integration',
    icon: 'ti-settings',
    path: '/integration'
  },
  {
    name: 'SMTP',
    icon: 'ti-email',
    path: '/smtp'
  }
];

export default navigations
