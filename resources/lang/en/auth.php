<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login' => [
        'title' => 'Login',
        'register' => 'Register',
        'email_placeholder' => 'Enter email',
        'password_placeholder' => 'Password',
        'forgot_password' => 'Forgot Your Password?',
        'button' => 'Login',
    ],
    'register' => [
        'title' => 'Register',
        'name_label' => 'Name',
        'email_label' => 'E-Mail Address',
        'password_label' => 'Password',
        'confirm_password_label' => 'Confirm Password',
        'button' => 'Register',
    ],
    'forgot_password' => [
        'title' => 'Reset Password',
        'email_label' => 'E-Mail Address',
        'button' => 'Send Password Reset Link',
    ],
    'reset_password' => [
        'title' => 'Reset Password',
        'email_label' => 'E-Mail Address',
        'password_label' => 'Password',
        'confirm_password_label' => 'Confirm Password',
        'button' => 'Reset Password',
    ],
];
