<?php

namespace Modules\Getresponse\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class GetResponseController extends Controller
{
    public function index()
    {
        return view('getresponse::index');
    }
}
