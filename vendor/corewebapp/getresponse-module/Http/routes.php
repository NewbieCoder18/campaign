<?php

/**
 * GetResponse
 */
Route::group(['prefix' => 'cwa', 'namespace' => 'Modules\GetResponse\Http\Controllers'], function () {
    Route::get('getresponse', 'GetResponseController@index');
});
