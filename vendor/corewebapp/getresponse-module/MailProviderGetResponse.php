<?php

namespace Modules\GetResponse;

use Modules\GetResponse\Helpers\GetResponseHelper;

class MailProviderGetResponse
{
    private $getresponse;
    private $api_key;

    public function __construct($apiKey)
    {
        $this->getresponse = new GetResponseHelper($apiKey);
        $this->api_key = $apiKey;
    }

    private function arrayToObject($array)
    {
        return (object)$array;
    }

    /**
    * Get all getresponse campaigns
    *
    * @return object
    */
    public function lists()
    {
        try {
            $lists = $this->getresponse->getCampaigns();

            //check the response
            if (isset($lists->httpStatus)) {
                return $lists;
            }

            $aList = [];
            foreach ($lists as $list) {
                $oList = new \stdClass();
                $oList->id = $list->campaignId;
                $oList->name = $list->name;
                $aList[] = $oList;
            }

            return $this->arrayToObject($aList);
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
    * Subcribe new user to campaign
    *
    * @param $list Campaign ID
    * @param $email User Email
    * @param $name (Optional) Subscriber name
    * @return
    */
    public function subscribe($list, $email, $name = '')
    {
        try {
            $_name = trim($name);
            if (empty($_name)) {
                $_ = explode('@', $email);
                if (count($_) > 0) {
                    $_name = $_[0];
                }
            }

            return $this->getresponse->addContact([
                'name' => $_name,
                'email' => $email,
                'campaign' => ['campaignId' => $list],
                'dayOfCycle' => 0,
                'ipAddress' => \Request::ip()
            ]);
        } catch (Exception $e) {
            return $e;
        }
    }

    public function contacts($list, $perPage = 100, $page = 1)
    {
        try {
            $contacts = $this->getresponse->getContacts([
                'perPage' => $perPage,
                'page' => $page
            ]);
        } catch (\Exception $e) {
            return $e;
        }

        return (array) $contacts;
    }

    /**
    * Subcribe new user to campaign
    *
    * @param $email User Email
    * @param $name (Optional) Subscriber name
    * @return
    */
    public function unsubscribe($email)
    {
        try {
            $contact = $this->call('contacts?query[email]=' . $email . '&fields=contactId');
            if ($contact != null && count($contact) > 0) {
                foreach ($contact as $v) {
                    $contact_id = $v->contactId;
                }
                return $this->call('contacts/' . $contact_id, 'DELETE');
            }
            return null;
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
    * Get all tags
    *
    * @return
    */
    public function getTags()
    {
        try {
            $tags = $this->call('tags');
            return $tags;
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
    * Set tags
    *
    * @return
    */
    public function setTag($email, $tag_id)
    {
        try {
            $contact = $this->call('contacts?query[email]=' . $email . '&fields=contactId');
            if ($contact != null && count($contact) > 0) {
                foreach ($contact as $v) {
                    $contact_id = $v->contactId;
                }

                return $this->call('contacts/' . $contact_id, 'POST', [
                    'tags' => [
                        [
                            'tagId' => $tag_id,
                        ]
                    ]
                ]);
            }
            return null;
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
    * Set tags by name
    *
    * @return
    */
    public function setTagByName($email, $newTags)
    {
        try {
            $newTagsArr = explode(",", $newTags);

            //get old tags
            $tags = $this->getTags();
            if ($tags != null && count($tags) > 0) {
                $a = 0;

                foreach ($tags as $y) {
                    $a++;
                    if (!in_array($y->name, $newTagsArr)) {
                        for ($x = 0; $x < count($newTagsArr); $x++) {
                            if ($y->name != $newTagsArr[$x]) {
                                //post new tags
                                $this->PostNewTag($newTagsArr[$x]);
                            }
                        }
                    }
                }

                if ($a == 0) {
                    for ($x = 0; $x < count($newTagsArr); $x++) {
                        //post new tags
                        $this->PostNewTag($newTagsArr[$x]);
                    }
                }
            }

            //get new tags
            $tags = $this->getTags();
            if ($tags != null && count($tags) > 0) {
                $newArr = array();
                $a = 0;

                foreach ($tags as $y) {
                    $newArr[$a]['tagId'] = $y->tagId;
                    $a++;
                }

                $this->SetTagsToContact($email, $newArr);
            }
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
    * Remove tags by Name
    *
    * @return
    */
    public function removeTagByName($email, $removeTags)
    {
        try {
            $this->RemoveTag($removeTags);
        } catch (Exception $e) {
            return $e;
        }
    }

    public function SetTagsToContact($email, $tag_id)
    {
        $contact = $this->call('contacts?query[email]=' . $email . '&fields=contactId');
        if ($contact != null && count($contact) > 0) {
            foreach ($contact as $v) {
                $contact_id = $v->contactId;
            }

            $tags_param = [
                'tags' => $tag_id
            ];

            return $this->call('contacts/' . $contact_id, 'POST', $tags_param);
        }

        return null;
    }

    public function PostNewTag($tag)
    {
        return $this->call('tags', 'POST', ['name' => $tag]);
    }

    public function RemoveTag($removeTags)
    {
        $removeTagsArr = explode(",", $removeTags);

        //get old tags
        $tags = $this->getTags();

        foreach ($tags as $y) {
            if (in_array($y->name, $removeTagsArr)) {
                $this->call('tags/' . $y->tagId, 'DELETE');
            }
        }
    }

    /**
     * Curl run request
     *
     * @param null $api_method
     * @param string $http_method
     * @param array $params
     * @return mixed
     * @throws Exception
     */
    private function call($api_method = null, $http_method = 'GET', $params = array())
    {
        if (empty($api_method)) {
            return (object)array(
                'httpStatus' => '400',
                'code' => '1010',
                'codeDescription' => 'Error in external resources',
                'message' => 'Invalid api method'
            );
        }

        $params = json_encode($params);
        $url = 'https://api.getresponse.com/v3/' . $api_method;

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_ENCODING => 'gzip,deflate',
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT => 8,
            CURLOPT_HEADER => false,
            CURLOPT_USERAGENT => 'PHP GetResponse client 0.0.2',
            CURLOPT_HTTPHEADER => array('X-Auth-Token: api-key ' . $this->api_key, 'Content-Type: application/json'),
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
        );

        if ($http_method == 'POST') {
            $options[CURLOPT_POST] = 1;
            $options[CURLOPT_POSTFIELDS] = $params;
        } elseif ($http_method == 'DELETE') {
            $options[CURLOPT_CUSTOMREQUEST] = 'DELETE';
        }

        $curl = curl_init();
        curl_setopt_array($curl, $options);

        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        return (object)$response;
    }
}
