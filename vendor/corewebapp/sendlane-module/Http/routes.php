<?php

Route::group(['prefix' => 'sendlane', 'namespace' => 'Modules\Sendlane\Http\Controllers'], function () {
    Route::get('/', 'SendlaneController@index');
});
