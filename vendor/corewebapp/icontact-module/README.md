# Core Web App - iContact Module

## Installation

```bash
composer require corewebapp/icontact-module 1.0.x-dev
```

## How to Use

```bash
$mailProvider = new MailProvideriContact($appId, $password, $username);

//get list
$lists = $mailProvider->lists();
dd($lists);

//subscribe
$list_id = 1; //example only, please replace with list id that you can get from $lists
$email = "example@avectris.com";
$first_name = "First";
$last_name = "Last";
$mailProvider->subscribe($list_id, $email, $first_name, $last_name);
```
