<?php

Route::group(['prefix' => 'icontact', 'namespace' => 'Modules\IContact\Http\Controllers'], function () {
    Route::get('/', 'IContactController@index');
});
