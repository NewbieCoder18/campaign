<?php

namespace Modules\IContact;

use iContact\iContactApi;

class MailProvideriContact
{
    private $icontact;

    public function __construct($appId, $apiPassword, $apiUsername)
    {
        $this->icontact = iContactApi::getInstance()->setConfig(array(
            'appId'       => $appId,
            'apiPassword' => $apiPassword,
            'apiUsername' => $apiUsername
        ));
    }

    private function arrayToObject($array)
    {
        return (object)$array;
    }

    /**
    * Get all iContact lists
    */
    public function lists()
    {
        try {
            $lists = $this->arrayToObject($this->icontact->getLists());
            foreach ($lists as $list) {
                $oList = new \stdClass();
                $oList->id = $list->listId;
                $oList->name = $list->name;

                $aList[] = $oList;
            }
            return $this->arrayToObject($aList);
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
    * Subscribe new email to MinMimi list
    *
    * @param string $list List ID
    * @param string $email Email address
    * @param string $firstName Optional. Customer first name
    * @param string $lastName Optional. Customer last name
    * @return string
    */
    public function subscribe($list, $email, $firstName = '', $lastName = '')
    {
        try {
            $contact = $this->icontact->addContact($email, 'normal', null, $firstName, $lastName);
            $contactId =  $contact->contactId;

            return $this->arrayToObject($this->icontact->subscribeContactToList($contactId, $list, 'normal'));
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function contacts($list, $offset = 0, $limit = 100)
    {
        try {
            $contacts = $this->icontact->getContacts($list, $offset, $limit);
        } catch (\Exception $e) {
            return $e;
        }

        return $contacts;
    }
}
