<?php

namespace Modules\Aweber;

class MailProviderAweber
{
    private $consumerKey;
    private $consumerSecret;
    private $accessKey;
    private $accessSecret;

    private $account;

    /**
     * Constructors
     *
     * @param string $consumerKey
     * @param string $consumerSecret
     * @param string $accessKey
     * @param string $accessSecret
     */
    public function __construct($consumerKey, $consumerSecret, $accessKey, $accessSecret)
    {
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;
        $this->accessKey = $accessKey;
        $this->accessSecret = $accessSecret;

        $aweber = new \AWeberAPI($consumerKey, $consumerSecret);
        $this->account = $aweber->getAccount($accessKey, $accessSecret);
    }

    /**
     * Generate the credentials (consumer key, consumer secret, access key, access secret) from authorization code
     *
     * @param string $authorization_code Authorization Code
     * @return associative array consumer_key, consumer_secret, access_key, access_secret
     */
    public static function getCredentialsFromAuthorizationCode($authorization_code)
    {
        $auth = \AWeberAPI::getDataFromAweberID($authorization_code);
        if (count($auth) != 4) {
            //There is something wrong, so return null
            return;
        }

        $data = [
            'consumer_key'      => $auth[0],
            'consumer_secret'   => $auth[1],
            'access_key'        => $auth[2],
            'access_secret'     => $auth[3]
        ];

        return $data;
    }

    /**
     * Helper to convert array into object
     * @param array $array
     * @return Object
     */
    private function arrayToObject($array)
    {
        return (object)$array;
    }

    /**
     * Get all aweber list
     *
     * @return object
     */
    public function lists()
    {
        try {
            $lists = $this->account->lists->data['entries'];
            $aList = [];

            foreach ($lists as $list) {
                $oList = new \stdClass();
                $oList->id = $list['id'];
                $oList->name = $list['name'];
                $aList[] = $oList;
            }

            return $this->arrayToObject($aList);
        } catch (\AWeberAPIException $e) {
            return $e;
        }
    }

    /**
     * Subcribe new user to campaign
     *
     * @param $list_id Campaign ID
     * @param $email User Email
     * @param $name (Optional) Subscriber name
     * @param $tags (Optional) Subscriber Tags
     * @return
     */
    public function subscribe($list_id, $email, $name = '', $tags = [])
    {
        try {
            $account_id = $this->account->data["id"];
            $listURL = "/accounts/{$account_id}/lists/{$list_id}";
            $list = $this->account->loadFromUrl($listURL);

            $params = [
                'email' => $email,
                'name' => $name,
                'tags' => $tags
            ];

            $subscribers = $list->subscribers;
            return $subscribers->create($params);
        } catch (\AWeberAPIException $e) {
            return $e;
        }
    }
	
	public function unsubscribe($list_id, $email, $name)
    {
        try {
            $account_id = $this->account->data["id"];
            $listURL = "/accounts/{$account_id}/lists/{$list_id}";
            $list = $this->account->loadFromUrl($listURL);
			
			$params = [
                'email' => $email,
                'name' => $name,
				'status' => 'subscribed'
            ];

            $found_subscribers = $list->subscribers->find($params);
			
			foreach($found_subscribers as $subscriber) {
				return $subscriber->delete();
			}
			
        } catch (\AWeberAPIException $e) {
            return $e;
        }
    }
	
	public function addTags($list_id, $email, $name, $tags = [])
    {
        try {
            $account_id = $this->account->data["id"];
            $listURL = "/accounts/{$account_id}/lists/{$list_id}";
            $list = $this->account->loadFromUrl($listURL);
			
			$params = [
                'email' => $email,
                'name' => $name
            ];

            $found_subscribers = $list->subscribers->find($params);
			
			foreach($found_subscribers as $subscriber) {
				 $subscriber->tags = array(
					'add' => $tags
				);
				return $subscriber->save();
			}
			
        } catch (\AWeberAPIException $e) {
            return $e;
        }
    }
	
	public function removeTags($list_id, $email, $name, $tags = [])
    {
        try {
            $account_id = $this->account->data["id"];
            $listURL = "/accounts/{$account_id}/lists/{$list_id}";
            $list = $this->account->loadFromUrl($listURL);
			
			$params = [
                'email' => $email,
                'name' => $name
            ];

            $found_subscribers = $list->subscribers->find($params);
			
			foreach($found_subscribers as $subscriber) {
				 $subscriber->tags = array(
					'remove' => $tags
				);
				return $subscriber->save();
			}
			
        } catch (\AWeberAPIException $e) {
            return $e;
        }
    }

    public function contacts($list_id, $start = 0, $size = 100)
    {
        try {
            $account_id = $this->account->data['id'];
            $listURL = sprintf(
                '/accounts/%s/lists/%s/subscribers?ws.start=%s&ws.size=%s',
                $account_id,
                $list_id,
                $start,
                $size
            );
            $subscribers = $this->account->loadFromUrl($listURL);
        } catch (\AWeberAPIException $e) {
            return $e;
        }

        return $subscribers;
    }
}
