# Core Web App - AWeber Module

## Installation

```bash
composer require corewebapp/aweber-module 1.0.x-dev
```

## How to Use

```bash
$mailProvider = new MailProviderAweber($consumerKey, $consumerSecret, $accessKey, $accessSecret);

//get list
$lists = $mailProvider->lists();
dd($lists);

//subscribe
$list = 1; //example only, please replace with list id that you can get from $lists
$email = "example@avectris.com";
$name = "Example";
$mailProvider->subscribe($list, $email, $name);
```
