<?php

namespace Modules\Infusionsoft\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Infusionsoft\Helpers\ConfigHelper;

use Modules\Infusionsoft\Infusionsoft as InfusionsoftCWA;
use Infusionsoft\Infusionsoft;

class InfusionsoftController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
        $this->middleware('auth', ['except' => ['tag']]);
        $this->middleware('administrator', ['except' => ['tag']]);
    }

    public function index(Request $request)
    {
        $name = $request->input('name');
        $transactiontype = $request->input('transactiontype');
        $jvzooId = $request->input('jvzoo_id');
        $tag = $request->input('tag');

        $infusionsofts = (empty($name)) ? InfusionsoftCWA::where('name', '!=', '') : InfusionsoftCWA::where('name', $name);
        if (!empty($transactiontype)) {
            $infusionsofts = $infusionsofts->where('transactiontype', $transactiontype);
        }

        if (!empty($jvzooId)) {
            $infusionsofts = $infusionsofts->where('jvzoo_id', $jvzooId);
        }

        if (!empty($tag)) {
            $infusionsofts = $infusionsofts->where('tag', $tag);
        }

        $infusionsofts = $infusionsofts->paginate(10);
        return view('infusionsoft::index', compact('infusionsofts', 'request'));
    }

    public function authenticate(Request $request)
    {
        $config = ConfigHelper::config();

        $infusionsoft = new Infusionsoft(array(
            'clientId'     => $config->clientID,
            'clientSecret' => $config->clientSecret,
            'redirectUri'  => $config->redirectURI,
        ));

        if (!empty($infusionsoft->tokenserialized)) {
            // TODO
            // Check $row
            $infusionsoft->setToken(unserialize($row['tokenserialized']));
        }

        if (!empty($request->input('code')) && !$infusionsoft->getToken()) {
            $infusionsoft->requestAccessToken($request->input('code'));
        }

        if ($infusionsoft->getToken()) {
            // Save the serialized token to the current session for subsequent requests
            //$_SESSION['token'] = serialize($infusionsoft->getToken());
            $newtoken = serialize($infusionsoft->getToken());
            ConfigHelper::refreshtoken($newtoken);

            echo sprintf('Authentication renewed.<br/><br>This is your token: <br><br>%s', serialize($infusionsoft->getToken()));
        }

        echo sprintf('<a href="%s">Click here to authorize</a>', $infusionsoft->getAuthorizationUrl());
    }

    public function tag(Request $request, $email = '', $prodId = '', $name = '', $portalpassword = '')
    {
        $config = ConfigHelper::config();

        $infusionsoft = new Infusionsoft(array(
            'clientId'     => $config->clientID,
            'clientSecret' => $config->clientSecret,
            'redirectUri'  => $config->redirectURI,
        ));

        if (!empty($config->tokenserialized)) {
            $infusionsoft->setToken(unserialize($config->tokenserialized));

            //refresh the access token
            $infusionsoft->refreshAccessToken();

            $newtoken = serialize($infusionsoft->getToken());

            ConfigHelper::refreshtoken($newtoken);
            echo 'Token refreshed<hr/>';
        }

        if ($infusionsoft->getToken()) {
            // MAKE INFUSIONSOFT REQUEST
            if (!empty($request->email)) {
                $email = $request->email;
                $prodId = $request->prodID;
                $name = $request->name;
                $portalpassword = $request->portalpassword;
            }

            $namesplit = explode(" ", $name, 2);
            $firstname = $name; //default
            $lastname = ""; //default
            if (!empty($namesplit[0])) {
                $firstname = $namesplit[0];
            }

            if (!empty($namesplit[1])) {
                $lastname = $namesplit[1];
            }

            if (true || $_REQUEST['ctransaction'] == 'SALE') {
                $tagIDs[4504439436081] = 1292;
                $tagIDs[4504458336132] = 1292;
                $tagId = $tagIDs[$prodId];

                $data = [
                    'FirstName'         => $firstname,
                    'LastName'          => $lastname,
                    'Email'             => $email,
                    '_PortalPassword'   => $portalpassword,
                    '_AE20Password'     => $portalpassword
                ];

                $dupCheckType = 'Email';

                $contactId = $infusionsoft->contacts()->addWithDupCheck($data, $dupCheckType);
                echo sprintf('Contact added. ID: %s<br/>', $contactId);
                $infusionsoft->emails()->optIn($email, 'Contact added by JVZoo integration.');
                echo 'Contact set as opt-in.<br/>';
                $infusionsoft->contacts()->addToGroup($contactId, $tagId);
                echo sprintf('Contact tagged with tag: %s.<br/>', $tagId);
            }
        }
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  Request  $request
    * @return Response
    */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'transactiontype' => 'required|max:50',
            'jvzoo_id' => 'required|max:50',
            'tag' => 'required|max:50'
        ]);

        $infusionsoft = new InfusionsoftCWA;
        $infusionsoft->name = $request->name;
        $infusionsoft->transactiontype = $request->transactiontype;
        $infusionsoft->jvzoo_id = $request->jvzoo_id;
        $infusionsoft->tag = $request->tag;
        $infusionsoft->save();

        $redir = 'cwa/infusionsoft';
        if ($request->destination == 'create') {
            $redir = 'cwa/infusionsoft/' . $request->destination;
        }

        return redirect($redir)->with(
            'message',
            sprintf(
                'Infusionsoft integration <a href="%s">%s</a> was created.',
                url('cwa/infusionsoft', $infusionsoft->uniqueid()),
                $infusionsoft->name
            )
        );
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy(InfusionsoftCWA $infusionsoft)
    {
        $infusionsoft->delete();

        return redirect('cwa/infusionsoft')
            ->with('message', sprintf('Infusionsoft integration %s was trashed!', $infusionsoft->name));
    }
}
