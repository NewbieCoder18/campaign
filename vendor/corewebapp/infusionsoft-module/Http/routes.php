<?php
/**
 * Infusionsoft
 */
Route::group(['prefix' => 'cwa'], function () {
    Route::post(
        'infusionsoft/tag',
        'Modules\Infusionsoft\Http\Controllers\InfusionsoftController@tag'
    );

    Route::get(
        'infusionsoft/authenticate',
        'Modules\Infusionsoft\Http\Controllers\InfusionsoftController@authenticate'
    );

    Route::resource(
        'infusionsoft',
        'Modules\Infusionsoft\Http\Controllers\InfusionsoftController'
    );
});

Route::bind('infusionsoft', function ($value, $route) {
    $id = Hashids::decode($value)[0];
    return Modules\Infusionsoft\Infusionsoft::find($id);
});
