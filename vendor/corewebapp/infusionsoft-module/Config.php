<?php

namespace Modules\Infusionsoft;

use Illuminate\Database\Eloquent\Model;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Config extends Model
{
    use EloquentTrait;

    protected $guarded = [];
    protected $table = 'cwa_infusionsoft_config';
    protected $fillable = ['clientID', 'clientSecret', 'redirectURI', 'tokenserialized'];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }
}
