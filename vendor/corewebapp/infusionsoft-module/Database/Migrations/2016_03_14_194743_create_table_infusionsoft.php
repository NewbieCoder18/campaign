<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInfusionsoft extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('infusionsoft', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('transactiontype', 50);
            $table->string('jvzoo_id', 50);
            $table->integer('tag');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::drop('infusionsoft');
    }
}
