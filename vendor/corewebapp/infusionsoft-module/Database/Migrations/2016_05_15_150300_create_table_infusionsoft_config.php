<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInfusionsoftConfig extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('infusionsoft_config', function (Blueprint $table) {
            $table->increments('id');
            $table->string('clientID', 50);
            $table->string('clientSecret', 50);
            $table->string('redirectURI', 100);
            $table->string('tokenserialized', 1000);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::drop('infusionsoft_config');
    }
}
