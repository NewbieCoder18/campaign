<?php

namespace Modules\Infusionsoft\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class InfusionsoftDatabaseSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        Model::unguard();
        // $this->call("OthersTableSeeder");
    }
}
